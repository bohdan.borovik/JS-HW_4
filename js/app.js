let currency = [];
let now = new Date();
let year = now.getFullYear();
let month = now.getMonth();
let day = now.getDay();
month++;
month = (month <= 9) ? '0' + month : '' + month;
day = (day <= 9) ? '0' + day : '' + day;
let date = '' + year + month + day;
fetchData(date);

function renderCurrency(currency) {
    const htmlStr = currency.reduce(function(acc, current, index) {
        return acc + `<tr>
                    <td>${index + 1}</td>
                    <td>${current.digital}</td>
                    <td>${current.name}</td>
                    <td>${current.rate}</td>
                    <td>${current.letter}</td>
                    <td>${current.exchangedate}</td>
                </tr>`;
    }, '');
    document.querySelector('.table tbody').innerHTML = htmlStr || `<tr><td colspan="6" class="text-center">Немає даних</td></tr>`;
}

document.getElementById('date').onchange = function(event) {
    let value = event.currentTarget.value.replaceAll('-','');
    document.querySelector('#search').value = '';
    fetchData(value);
}

document.querySelector('#search').onkeyup = function(event) {
    let value = event.currentTarget.value.toLowerCase().trim();
    let filteredCurrency = currency.filter(function(current) {
        let name = current.name.toLowerCase();
        let letter = current.letter.toLowerCase();
        return name.includes(value) || letter.includes(value);
    });
    renderCurrency(filteredCurrency);
}

function fetchData(date){
    fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=' + date + '&json').then(function(data){
        return data.json();
    }).then(function(data){
        currency = data.map(function(current){
            return {
                digital: current.r030,
                name: current.txt,
                rate: current.rate,
                letter: current.cc,
                exchangedate: current.exchangedate
            };
        });
        renderCurrency(currency);
    });
}